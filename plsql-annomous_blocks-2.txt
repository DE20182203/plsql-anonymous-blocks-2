1)SQL> declare
  2  begin
  3  for i in 0..50 loop
  4  if mod(i,2)!=0 then
  5  dbms_output.put_line(i);
  6  end if;
  7  end loop;
  8  end;
  9  /

2)SQL> declare
  2  begin
  3  for i in 1..25 loop
  4  if i=19 then
  5  dbms_output.put_line('Exiting from the loop');
  6  exit;
  7  end if;
  8  dbms_output.put_line(i);
  9  end loop;
 10  end;
 11  /
3)SQL>  declare
  2  v_diff_sal number;
  3      begin
  4      select max(sal)-min(sal) into v_diff_sal from emp;
  5  dbms_output.put_line(v_diff_sal);
  6     end;
  7      /

4)SQL> declare
  2  i number:=10;
  3  j number:=20;
  4  begin
  5  dbms_output.put_line('the outer block');
  6  dbms_output.put_line(i||''||j);
  7  declare
  8  i number:=20;
  9  begin
 10  dbms_output.put_line('the inner block');
 11  dbms_output.put_line(i);
 12  dbms_output.put_line(i||' at the inner block');
 13  end;
 14  dbms_output.put_line(i||'at the outer block');
 15  end;
 16  /

5)
SQL> variable val1 number;
SQL> variable val2 number;
SQL>     declare
  2      val1 number := 5;
  3      val2 number := 6;
  4       begin
  5       :val1:= 10;
  6      :val2:= 20;
  7      dbms_output.put_line('Bind_variable:'||:val1||'local_variable:'||val1);
  8  dbms_output.put_line('Bind_variable:'||:val2||'local_variable:'||val2);
  9     end;
 10  /








